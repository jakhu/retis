# retisci [![Code Climate](https://codeclimate.com/github/jakhu/retis-ci/badges/gpa.svg)](https://codeclimate.com/github/jakhu/retis-ci) [![Dependency Status](https://david-dm.org/jakhu/retis-ci.svg)](https://david-dm.org/jakhu/retis-ci) [![devDependency Status](https://david-dm.org/jakhu/retis-ci/dev-status.svg)](https://david-dm.org/jakhu/retis-ci#info=devDependencies)
The advanced build service for everyone - including dudes!

**NOTICE:** Currently in active development. Not all features may be available. There may be bugs; if you find a bug, please file an issue.

# Getting Started
```bash
$ npm install -g jakhu/retis-ci
```
# Building
```bash
# Compile libs
coffee --bare -o lib -c src
# Compile bin
coffee --bare -o bin -c src/bin
```

# Tests
TODO

# Creating your first project
TODO

# Making plugins
TODO
